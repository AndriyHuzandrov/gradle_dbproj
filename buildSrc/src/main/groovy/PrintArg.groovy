import org.gradle.api.DefaultTask
import org.gradle.api.tasks.TaskAction

class PrinttArg extends DefaultTask {
    String tool

    @TaskAction
    void printTArg() {
        switch (tool) {
            case 'java':
                println System.getProperty("java.version")
                break
            case 'groovy':
                println GroovySystem.version
                break
            default:
                throw new IllegalArgumentException("Unknown tool")
        }
    }
}

