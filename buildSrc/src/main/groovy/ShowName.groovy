import org.gradle.api.DefaultTask
import org.gradle.api.tasks.TaskAction
import org.gradle.api.tasks.options.Option

class ShowName extends DefaultTask{
    private String name

    @Option(option = "name", description = "Is added to output")
    public void setName(String n) {
        name = n
    }

    @TaskAction
    public void printName() {
        println 'Hello ' + name
    }
}
