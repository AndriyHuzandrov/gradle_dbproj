package com.epam.grdlproj.bus.service;

import com.epam.grdlproj.model.City;
import com.epam.grdlproj.model.Department;
import com.epam.grdlproj.model.dao.CityDao;
import com.epam.grdlproj.model.dao.DAO;
import com.epam.grdlproj.model.dao.DeptDao;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

public class DepartService implements Fetchable<Department> {

  public List<Department> findAll() throws SQLException {
    DAO<Department, Integer> data = new DeptDao();
    List<Department> output = data.getAll();
    data.closeConnection();
    return output;
  }

  public boolean addDepartment(Department d) throws SQLException {
    DAO<Department, Integer> data = new DeptDao();
    boolean output = data.create(d);
    data.closeConnection();
    return output;
  }

  public Department fetchDepById(Integer id) throws SQLException {
    DAO<Department, Integer> data = new DeptDao();
    Optional<Department> output = data.getById(id);
    data.closeConnection();
    return output.orElseThrow(IllegalArgumentException::new);
  }

  public boolean changeDepartment(Department d) throws SQLException {
    DAO<Department, Integer> data = new DeptDao();
    boolean output = data.update(d);
    data.closeConnection();
    return output;
  }

  public boolean removeCity(Integer id) throws SQLException {
    DAO<City, Integer> data = new CityDao();
    boolean output = data.delete(id);
    data.closeConnection();
    return output;
  }
}
