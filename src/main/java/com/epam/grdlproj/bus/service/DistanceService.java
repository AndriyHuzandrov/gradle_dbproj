package com.epam.grdlproj.bus.service;

import com.epam.grdlproj.model.Distance;
import com.epam.grdlproj.model.Price;
import com.epam.grdlproj.model.dao.DAO;
import com.epam.grdlproj.model.dao.DistanceDao;
import com.epam.grdlproj.model.dao.PriceDao;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

public class DistanceService implements Fetchable<Distance> {

  public List<Distance> findAll() throws SQLException {
    DAO<Distance, Integer> data = new DistanceDao();
    List<Distance> output = data.getAll();
    data.closeConnection();
    return output;
  }

  public boolean addPRoute(Distance d) throws SQLException {
    DAO<Distance, Integer> data = new DistanceDao();
    boolean output = data.create(d);
    data.closeConnection();
    return output;
  }

  public Distance fetchRouteById(Integer id) throws SQLException {
    DAO<Distance, Integer> data = new DistanceDao();
    Optional<Distance> output = data.getById(id);
    data.closeConnection();
    return output.orElseThrow(IllegalArgumentException::new);
  }

  public boolean changeRoute(Distance d) throws SQLException {
    DAO<Distance, Integer> data = new DistanceDao();
    boolean output = data.update(d);
    data.closeConnection();
    return output;
  }

  public boolean removeRoute(Integer id) throws SQLException {
    DAO<Price, Integer> data = new PriceDao();
    boolean output = data.delete(id);
    data.closeConnection();
    return output;
  }
}
