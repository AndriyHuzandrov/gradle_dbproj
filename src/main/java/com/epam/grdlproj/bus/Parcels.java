package com.epam.grdlproj.bus;

import static com.epam.grdlproj.txtconst.TxtConsts.*;
import com.epam.grdlproj.bus.service.ClientService;
import com.epam.grdlproj.bus.service.DepartService;
import com.epam.grdlproj.bus.service.DistanceService;
import com.epam.grdlproj.model.Client;
import com.epam.grdlproj.model.Parcel;
import com.epam.grdlproj.view.Displayable;
import com.epam.grdlproj.view.StdView;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.Optional;

public class Parcels {
  private static final BufferedReader br;
  private static Displayable monitor;

  private Parcels() {
  }
  static {
    br = new BufferedReader(new InputStreamReader(System.in));
    monitor = new StdView();
  }

  public static Parcel create() {
    ClientService service = new ClientService();
    Client sender;
    Client recipient;
    Parcel currParcel = new Parcel();
    try {
      sender = service.fetchClientById(validateClientByMobile(service).getClientId());
      currParcel.setSender(sender.getClientId());
      currParcel.setSenderName(sender.getClName() + " " + sender.getClSurname());
      recipient = service.fetchClientById(validateClientByMobile(service).getClientId());
      currParcel.setRecipient(recipient.getClientId());
      currParcel.setRecipeName(recipient.getClName() + " " +recipient.getClSurname());
      DepartService departService = new DepartService();
      departService
          .findAll()
          .forEach(d -> monitor.display(d.toString()));
      monitor.display(ASK_RECORD_NUM);
      currParcel.setFromDep(
          departService.fetchDepById(Integer.parseInt(br.readLine())).getDepId()
      );
      monitor.display(ASK_WEIGHT);
      currParcel.setWeight(Double.parseDouble(br.readLine()));
      monitor.display(ASK_VOL);
      currParcel.setVol(Double.parseDouble(br.readLine()));
      DistanceService distService = new DistanceService();
      distService
          .findAll()
          .forEach(d -> monitor.display(d.toString()));
      monitor.display(ASK_RECORD_NUM);
      currParcel.setRoute(
          distService.fetchRouteById(Integer.parseInt(br.readLine())).getRouteId()
      );
      departService
          .findAll()
          .forEach(d -> monitor.display(d.toString()));
      monitor.display(ASK_RECORD_NUM);
      currParcel.setToDepartment(
          departService.fetchDepById(Integer.parseInt(br.readLine())).getDepId()
      );
    } catch (IOException e) {
      appLog.error(CONS_INP_ERROR);
    } catch (IllegalArgumentException e) {
      appLog.warn(WRONG_PHONE);
      create();
    } catch (SQLException e) {
      appLog.error("SQLException: " + e.getMessage());
      appLog.error("SQLState: " + e.getSQLState());
    }
    return currParcel;
  }

  private static Client addClient(String phone, ClientService service) {
    Client client = new Client();
    try {
      monitor.display(ASK_NAME);
      client.setClName(br.readLine().trim());
      monitor.display(ASK_SURNAME);
      client.setClSurname(br.readLine().trim());
      client.setMobile(phone);
      monitor.display(ASK_PASSPORT);
      client.setPassport(br.readLine().trim());
      service.addClient(client);
      client = service.fetchClientById(phone).orElseThrow(IllegalArgumentException::new);
    } catch (IOException e) {
      appLog.error(CONS_INP_ERROR);
    } catch (SQLException e) {
      appLog.error("SQLException: " + e.getMessage());
      appLog.error("SQLState: " + e.getSQLState());
    }
    return client;
  }

  private static Client validateClientByMobile(ClientService service)
      throws SQLException, IOException , IllegalArgumentException {
    monitor.display(ASK_PHONE);
    String phone = br.readLine();
    if(!phone.matches("^0\\d{9}")) {
      throw new IllegalArgumentException();
    }
    Optional<Client> client =
         service
        .findAll()
        .stream()
        .filter(c -> c.getMobile().equals(phone))
        .findFirst();
    return client.orElseGet(() -> addClient(phone, service));
  }
}
