package com.epam.grdlproj.menu;

import com.epam.grdlproj.UI.UI;
import java.util.HashMap;
import java.util.LinkedHashMap;

public class MainMenu extends Menu {

  HashMap<String, Performable> prepareExeMenu() {
    HashMap<String, Performable> menu = new LinkedHashMap<>();
    menu.put("0", this::exit);
    menu.put("1", this::makeParcelProcess);
    menu.put("2", this::makeParcelTrack);
    menu.put("3", this::makeMaintain);
    return menu;
  }
  private void makeParcelProcess() {
    Menu m = UI.newParcelProcesMenu();
    m.getChoice();
  }

  private void makeParcelTrack() {
    Menu m = UI.newClientLoginMenu();
    m.getChoice();
  }

  private void makeMaintain() {
    Menu m = UI.newMaintainMenu();
    m.getChoice();
  }
}
