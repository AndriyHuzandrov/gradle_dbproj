package com.epam.grdlproj.menu.routeene;

import static com.epam.grdlproj.txtconst.TxtConsts.SEL_MENU_ITM;
import static com.epam.grdlproj.txtconst.TxtConsts.appLog;

import java.util.List;

public abstract class Showable {
  List<String> menu;

  public Showable() {
    menu = fillMenu();
  }
  abstract List<String> fillMenu();

  public void show() {
    appLog.info("\n");
    menu
        .stream()
        .map(i -> i + "\n")
        .forEach(appLog::info);
    appLog.info(SEL_MENU_ITM);
  }
}
