package com.epam.grdlproj.menu;

import static com.epam.grdlproj.txtconst.TxtConsts.*;
import com.epam.grdlproj.UI.UI;
import com.epam.grdlproj.bus.service.CityService;
import com.epam.grdlproj.bus.service.DepartService;
import com.epam.grdlproj.bus.service.DistanceService;
import com.epam.grdlproj.bus.service.DistrictService;
import com.epam.grdlproj.bus.service.PriceService;
import com.epam.grdlproj.model.City;
import com.epam.grdlproj.model.Department;
import com.epam.grdlproj.model.Distance;
import com.epam.grdlproj.model.District;
import com.epam.grdlproj.model.Price;
import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Optional;

public class MaintainMenu extends Menu {

  HashMap<String, Performable> prepareExeMenu() {
    HashMap<String, Performable> menu = new LinkedHashMap<>();
    menu.put("0", this::exit);
    menu.put("1", this::changeDistrict);
    menu.put("2", this::changeCity);
    menu.put("3", this::changeDistance);
    menu.put("4", this::changeDep);
    menu.put("5", this::changePrice);
    return menu;
  }

  private void changeDistrict() {
    DistrictService service = new DistrictService();
    try {
      appLog.info(SEPARATOR);
      service
          .findAll()
          .forEach(d -> appLog.info(d.toString()));
      appLog.info(SEPARATOR);
    } catch (SQLException e) {
      appLog.error("SQLException: " + e.getMessage());
      appLog.error("SQLState: " + e.getSQLState());
    }
    appLog.info(MAINTAIN_CHOICE);
    try {
      switch (checkOperation().orElseThrow(IllegalArgumentException::new)) {
        case "a":
          District item = new District();
          appLog.info(ASK_NAME);
          try {
            item.setDistrName(br.readLine().trim());
            if(!service.addDistrict(item)) {
              throw new IllegalStateException();
            } else {
              changeDistrict();
            }
          } catch (SQLException e) {
            appLog.error("SQLException: " + e.getMessage());
            appLog.error("SQLState: " + e.getSQLState());
          } catch (IllegalStateException e) {
            appLog.warn(NO_DATA_SET);
            changeDistrict();
          } catch (IOException e) {
            appLog.error(CONS_INP_ERROR);
          }
          break;
        case "c":
          try {
            appLog.info(ASK_RECORD_NUM);
            item = service.fetchDistrictById(Integer.parseInt(br.readLine().trim()));
            appLog.info(ASK_NAME);
            item.setDistrName(br.readLine().trim());
            if(service.changeDistrict(item)) {
              appLog.info(item.toString() + " updated successfully\n");
            }
          } catch (SQLException e) {
            appLog.error("SQLException: " + e.getMessage());
            appLog.error("SQLState: " + e.getSQLState());
          } catch (IOException e) {
            appLog.error(CONS_INP_ERROR);
          } catch (IllegalArgumentException e) {
            appLog.error(NO_RECORD_FOUND);
          }
          break;
        case "d":
          appLog.info(ASK_RECORD_NUM);
          try {
            if(service.removeDistrict(Integer.parseInt(br.readLine().trim()))) {
              appLog.info(DELETE_SUCS);
            }
          } catch (SQLException e) {
            appLog.error("SQLException: " + e.getMessage());
            appLog.error("SQLState: " + e.getSQLState());
          } catch (IOException e) {
            appLog.error(CONS_INP_ERROR);
          }
          break;
          default:
            this.getChoice();
            break;
      }
    } catch (IllegalArgumentException e) {
      appLog.error(BAD_MENU_ITM);
      changeDistrict();
    }

  }
  private void changeCity() {
    CityService service = new CityService();
    try {
      appLog.info(SEPARATOR);
      service
          .findAll()
          .forEach(d -> appLog.info(d.toString()));
      appLog.info(SEPARATOR);
    } catch (SQLException e) {
      appLog.error("SQLException: " + e.getMessage());
      appLog.error("SQLState: " + e.getSQLState());
    }
    appLog.info(MAINTAIN_CHOICE);
    try {
      switch (checkOperation().orElseThrow(IllegalArgumentException::new)) {
        case "a":
          City item = new City();
          try {
            if(!service.addCity(prepareCityInfo(item))) {
              throw new IllegalStateException();
            } else {
              changeCity();
            }
          } catch (SQLException e) {
            appLog.error("SQLException: " + e.getMessage());
            appLog.error("SQLState: " + e.getSQLState());
          } catch (IllegalStateException e) {
            appLog.warn(NO_DATA_SET);
            changeDistrict();
          } catch (IOException e) {
            appLog.error(CONS_INP_ERROR);
          }
          break;
        case "c":
          try {
            appLog.info(ASK_RECORD_NUM);
            item = service.fetchCityById(Integer.parseInt(br.readLine().trim()));
            if(service.changeCity(prepareCityInfo(item))) {
              appLog.info(item.toString() + " updated successfully\n");
            }
          } catch (SQLException e) {
            appLog.error("SQLException: " + e.getMessage());
            appLog.error("SQLState: " + e.getSQLState());
          } catch (IOException e) {
            appLog.error(CONS_INP_ERROR);
          } catch (IllegalArgumentException e) {
            appLog.error(NO_RECORD_FOUND);
          }
          break;
        case "d":
          appLog.info(ASK_RECORD_NUM);
          try {
            if(service.removeCity(Integer.parseInt(br.readLine().trim()))) {
              appLog.info(DELETE_SUCS);
            }
          } catch (SQLException e) {
            appLog.error("SQLException: " + e.getMessage());
            appLog.error("SQLState: " + e.getSQLState());
          } catch (IOException e) {
            appLog.error(CONS_INP_ERROR);
          }
          break;
        default:
          this.getChoice();
          break;
      }
    } catch (IllegalArgumentException e) {
      appLog.error(BAD_MENU_ITM);
      changeDistrict();
    }
  }
  private void changeDistance() {
    DistanceService service = new DistanceService();
    try {
      appLog.info(SEPARATOR);
      service
          .findAll()
          .forEach(d -> appLog.info(d.toString()));
      appLog.info(SEPARATOR);
    } catch (SQLException e) {
      appLog.error("SQLException: " + e.getMessage());
      appLog.error("SQLState: " + e.getSQLState());
    }
    appLog.info(MAINTAIN_CHOICE);
    try {
      switch (checkOperation().orElseThrow(IllegalArgumentException::new)) {
        case "a":
          Distance item = new Distance();
          try {
            if(!service.addPRoute(prepareDistanceInfo(item))) {
              throw new IllegalStateException();
            } else {
              changeDistance();
            }
          } catch (SQLException e) {
            appLog.error("SQLException: " + e.getMessage());
            appLog.error("SQLState: " + e.getSQLState());
          } catch (IllegalStateException e) {
            appLog.warn(NO_DATA_SET);
            changeDep();
          } catch (IOException e) {
            appLog.error(CONS_INP_ERROR);
          }
          break;
        case "c":
          try {
            appLog.info(ASK_RECORD_NUM);
            item = service.fetchRouteById(Integer.parseInt(br.readLine().trim()));
            if(service.changeRoute(prepareDistanceInfo(item))) {
              appLog.info(item.toString() + " updated successfully\n");
            }
          } catch (SQLException e) {
            appLog.error("SQLException: " + e.getMessage());
            appLog.error("SQLState: " + e.getSQLState());
          } catch (IOException e) {
            appLog.error(CONS_INP_ERROR);
          } catch (IllegalArgumentException e) {
            appLog.error(NO_RECORD_FOUND);
          }
          break;
        case "d":
          appLog.info(ASK_RECORD_NUM);
          try {
            if(service.removeRoute(Integer.parseInt(br.readLine().trim()))) {
              appLog.info(DELETE_SUCS);
            }
          } catch (SQLException e) {
            appLog.error("SQLException: " + e.getMessage());
            appLog.error("SQLState: " + e.getSQLState());
          } catch (IOException e) {
            appLog.error(CONS_INP_ERROR);
          }
          break;
        default:
          this.getChoice();
          break;
      }
    } catch (IllegalArgumentException e) {
      appLog.error(BAD_MENU_ITM);
      changeDistance();
    }
  }

  private void changeDep() {
    DepartService service = new DepartService();
    try {
      appLog.info(SEPARATOR);
      service
          .findAll()
          .forEach(d -> appLog.info(d.toString()));
      appLog.info(SEPARATOR);
    } catch (SQLException e) {
      appLog.error("SQLException: " + e.getMessage());
      appLog.error("SQLState: " + e.getSQLState());
    }
    appLog.info(MAINTAIN_CHOICE);
    try {
      switch (checkOperation().orElseThrow(IllegalArgumentException::new)) {
        case "a":
          Department item = new Department();
          try {
            if(!service.addDepartment(prepareDepInfo(item))) {
              throw new IllegalStateException();
            } else {
              changeDep();
            }
          } catch (SQLException e) {
            appLog.error("SQLException: " + e.getMessage());
            appLog.error("SQLState: " + e.getSQLState());
          } catch (IllegalStateException e) {
            appLog.warn(NO_DATA_SET);
            changeDep();
          } catch (IOException e) {
            appLog.error(CONS_INP_ERROR);
          }
          break;
        case "c":
          try {
            appLog.info(ASK_RECORD_NUM);
            item = service.fetchDepById(Integer.parseInt(br.readLine().trim()));
            if(service.changeDepartment(prepareDepInfo(item))) {
              appLog.info(item.toString() + " updated successfully\n");
            }
          } catch (SQLException e) {
            appLog.error("SQLException: " + e.getMessage());
            appLog.error("SQLState: " + e.getSQLState());
          } catch (IOException e) {
            appLog.error(CONS_INP_ERROR);
          } catch (IllegalArgumentException e) {
            appLog.error(NO_RECORD_FOUND);
          }
          break;
        case "d":
          appLog.info(ASK_RECORD_NUM);
          try {
            if(service.removeCity(Integer.parseInt(br.readLine().trim()))) {
              appLog.info(DELETE_SUCS);
            }
          } catch (SQLException e) {
            appLog.error("SQLException: " + e.getMessage());
            appLog.error("SQLState: " + e.getSQLState());
          } catch (IOException e) {
            appLog.error(CONS_INP_ERROR);
          }
          break;
        default:
          this.getChoice();
          break;
      }
    } catch (IllegalArgumentException e) {
      appLog.error(BAD_MENU_ITM);
      changeDistrict();
    }
  }

  private void changePrice() {
    PriceService service = new PriceService();
    try {
      appLog.info(SEPARATOR);
      service
          .findAll()
          .forEach(p -> appLog.info(p.toString()));
      appLog.info(SEPARATOR);
    } catch (SQLException e) {
      appLog.error("SQLException: " + e.getMessage());
      appLog.error("SQLState: " + e.getSQLState());
    }
    appLog.info(MAINTAIN_CHOICE);
    try {
      switch (checkOperation().orElseThrow(IllegalArgumentException::new)) {
        case "a":
          Price item = new Price();
          try {
            if(!service.addPrice(preparePriceInfo(item))) {
              throw new IllegalStateException();
            } else {
              changePrice();
            }
          } catch (SQLException e) {
            appLog.error("SQLException: " + e.getMessage());
            appLog.error("SQLState: " + e.getSQLState());
          } catch (IllegalStateException e) {
            appLog.warn(NO_DATA_SET);
            changePrice();
          } catch (IOException e) {
            appLog.error(CONS_INP_ERROR);
          }
          break;
        case "c":
          try {
            appLog.info(ASK_RECORD_NUM);
            item = service.fetchPriceByCat(Integer.parseInt(br.readLine().trim()));
            if(service.changePrice(preparePriceInfo(item))) {
              appLog.info(item.toString() + " updated successfully\n");
            }
          } catch (SQLException e) {
            appLog.error("SQLException: " + e.getMessage());
            appLog.error("SQLState: " + e.getSQLState());
          } catch (IOException e) {
            appLog.error(CONS_INP_ERROR);
          } catch (IllegalArgumentException e) {
            appLog.error(NO_RECORD_FOUND);
          }
          break;
        case "d":
          appLog.info(ASK_RECORD_NUM);
          try {
            if(service.removePrice(Integer.parseInt(br.readLine().trim()))) {
              appLog.info(DELETE_SUCS);
            }
          } catch (SQLException e) {
            appLog.error("SQLException: " + e.getMessage());
            appLog.error("SQLState: " + e.getSQLState());
          } catch (IOException e) {
            appLog.error(CONS_INP_ERROR);
          }
          break;
        default:
          this.getChoice();
          break;
      }
    } catch (IllegalArgumentException e) {
      appLog.error(BAD_MENU_ITM);
      changePrice();
    }
  }

  void exit() {
    Menu m = UI.newMainMenu();
    m.getChoice();
  }

  private Optional<String> checkOperation() {
    Optional<String> output = Optional.empty();
    try {
      String choice = br.readLine().trim().toLowerCase();
      if(!choice.matches("a|c|d|e")) {
        throw new IllegalArgumentException();
      }
      output = Optional.of(choice);
    } catch (IOException e) {
      appLog.error(CONS_INP_ERROR);
    } catch (IllegalArgumentException e) {
      appLog.warn(BAD_MENU_ITM);
      checkOperation();
    }
    return output;
  }

  private Department prepareDepInfo(Department item) throws SQLException, IOException{
    appLog.info(ASK_D_NUMBER);
    item.setDepNum(Integer.parseInt(br.readLine().trim()));
    new CityService()
        .findAll()
        .forEach(d -> appLog.info(d.toString()));
    appLog.info(ASK_CITY);
    item.setCity(Integer.parseInt(br.readLine().trim()));
    appLog.info(ASK_ADRESS);
    item.setAddress(br.readLine().trim());
    return item;
  }

  private City prepareCityInfo(City item) throws SQLException, IOException {
    appLog.info(ASK_NAME);
    item.setCityName(br.readLine().trim());
    new DistrictService()
        .findAll()
        .forEach(d -> appLog.info(d.toString()));
    appLog.info(ASK_DISTR);
    item.setDistr(Integer.parseInt(br.readLine().trim()));
    return item;
  }

  private Price preparePriceInfo(Price item) throws IOException {
    appLog.info(ASK_PRICE_CAT);
    item.setCategory(Integer.parseInt(br.readLine().trim()));
    appLog.info(ASK_PRICE);
    item.setPrice(Double.parseDouble(br.readLine().trim()));
    return item;
  }

  private Distance prepareDistanceInfo(Distance distance) throws SQLException, IOException {
    new CityService()
        .findAll()
        .forEach(d -> appLog.info(d.toString()));
    appLog.info(ASK_R_START);
    distance.setFromCity(Integer.parseInt(br.readLine().trim()));
    appLog.info(ASK_R_DESTIN);
    distance.setToCity(Integer.parseInt(br.readLine().trim()));
    appLog.info(ASK_DIST);
    distance.setDistance(Integer.parseInt(br.readLine().trim()));
    return distance;
  }
}
