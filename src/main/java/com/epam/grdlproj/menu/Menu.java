package com.epam.grdlproj.menu;

import static com.epam.grdlproj.txtconst.TxtConsts.*;
import com.epam.grdlproj.control.Control;
import com.epam.grdlproj.menu.routeene.Showable;
import com.epam.grdlproj.view.Displayable;
import com.epam.grdlproj.view.StdView;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Map;


abstract public class Menu {
  BufferedReader br;
  Map<String, Performable> exeMenu;
  Control appControl;
  Showable txtMenu;
  Displayable monitor;
  String parcel_id;
  String client_id;

  Menu() {
    monitor = new StdView();
    appControl = new Control();
    br = new BufferedReader(new InputStreamReader(System.in));
    exeMenu = prepareExeMenu();
  }

  public String getParcel_id() {
    return parcel_id;
  }

  public String getClient_id() {
    return client_id;
  }

  public void setParcel_id(String parcel_id) {
    this.parcel_id = parcel_id;
  }

  public void setClient_id(String client_id) {
    this.client_id = client_id;
  }

  public void setTxtMenu(Showable txtMenu) {
    this.txtMenu = txtMenu;
  }

  public void getChoice() {
    int menuLastItem = exeMenu.size() - 1;
    txtMenu.show();
    try {
      String choice = br.readLine().trim();
      if(choice.matches("[0-" + menuLastItem + "]")) {
        processChoice(choice);
      } else {
        throw new IllegalArgumentException();
      }
    } catch (IOException e) {
      appLog.error(CONS_INP_ERROR);
    } catch (IllegalArgumentException e) {
      appLog.warn(BAD_MENU_ITM);
      getChoice();
    }
  }

  void processChoice(String c) {
    exeMenu
        .entrySet()
        .stream()
        .filter(i -> i.getKey().equals(c))
        .forEach(i -> i.getValue().perform());
    showPause();
    getChoice();
  }

  abstract Map<String, Performable> prepareExeMenu();

  void exit() {
    appControl.exitApp();
  }

  void showPause() {
    appLog.info(PAUSE_MSG);
    try {
      br.readLine();
    } catch (IOException e) {
      appLog.error(CONS_INP_ERROR);
    }
  }
}
