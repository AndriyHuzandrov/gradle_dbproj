package com.epam.grdlproj.menu;

@FunctionalInterface
public interface Performable {
  void perform();
}
