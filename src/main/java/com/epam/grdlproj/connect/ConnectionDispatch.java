package com.epam.grdlproj.connect;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class ConnectionDispatch implements Connectible {
  private static int connNums = 5;
  private List<Connection> connAvailable;
  private List<Connection> connUsed;

  public static ConnectionDispatch create(String url, String user, String paswd)
                                    throws SQLException {
    List<Connection> cons = new ArrayList<>(connNums);
    for(int i = 0; i < connNums; i++) {
      cons.add(createConnection(url, user, paswd));
    }
    return new ConnectionDispatch(cons);
  }

  private ConnectionDispatch(List<Connection> l) {
    connUsed = new LinkedList<>();
    connAvailable = l;
  }

  public boolean releaseConnection(Connection c) {
    connAvailable.add(c);
    return connUsed.remove(c);
  }

  public Connection getConnection() {
    Connection con = connAvailable.remove(connAvailable.size() - 1);
    connUsed.add(con);
    return con;
  }

  private static Connection createConnection(String url, String user, String paswd)
                                                                              throws SQLException {
    return DriverManager.getConnection(url, user, paswd);
  }

  public void shutdown() throws SQLException {
    connUsed.forEach(this::releaseConnection);
    for(Connection c : connAvailable) {
      c.close();
    }
    connAvailable.clear();
  }

}
