package com.epam.grdlproj.model;

import static com.epam.grdlproj.txtconst.TxtConsts.*;

public class Client {
  private Integer clientId;
  private String clName;
  private String clSurname;
  private String mobile;
  private String passport;

  public Integer getClientId() {
    return clientId;
  }

  public void setClientId(Integer clientId) {
    this.clientId = clientId;
  }

  public String getClName() {
    return clName;
  }

  public void setClName(String clName) {
    this.clName = clName;
  }

  public String getClSurname() {
    return clSurname;
  }

  public void setClSurname(String clSurname) {
    this.clSurname = clSurname;
  }

  public String getMobile() {
    return mobile;
  }

  public void setMobile(String mobile) {
    this.mobile = mobile;
  }

  public String getPassport() {
    return passport;
  }

  public void setPassport(String passport) {
    this.passport = passport;
  }

  public String toString() {
    return String.format(CLIENT_FORMAT, getClName(), getClSurname(), getMobile());
  }
}
