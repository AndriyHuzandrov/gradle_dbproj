package com.epam.grdlproj.model;

import static com.epam.grdlproj.txtconst.TxtConsts.*;

public class City {
  private Integer cityId;
  private String cityName;
  private Integer distr;
  private String inDistr;

  public Integer getCityId() {
    return cityId;
  }

  public void setCityId(Integer cityId) {
    this.cityId = cityId;
  }

  public String getCityName() {
    return cityName;
  }

  public void setCityName(String cityName) {
    this.cityName = cityName;
  }

  public Integer getDistr() {
    return distr;
  }

  public void setDistr(Integer distr) {
    this.distr = distr;
  }

  public String getInDistr() {
    return inDistr;
  }

  public void setInDistr(String inDistr) {
    this.inDistr = inDistr;
  }
  public String toString() {
    return String.format(CITY_FORMAT, getCityId(), getCityName(), getInDistr());
  }
}
