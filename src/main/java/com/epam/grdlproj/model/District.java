package com.epam.grdlproj.model;

import static com.epam.grdlproj.txtconst.TxtConsts.*;

public class District {
  private Integer distId;
  private String distrName;

  public Integer getDistId() {
    return distId;
  }

  public void setDistId(Integer distId) {
    this.distId = distId;
  }

  public String getDistrName() {
    return distrName;
  }

  public void setDistrName(String distrName) {
    this.distrName = distrName;
  }

  @Override
  public String toString() {
    return String.format(DISTR_FORMAT, getDistId(), getDistrName());
  }
}
