package com.epam.grdlproj.model;

import static com.epam.grdlproj.txtconst.TxtConsts.*;

import java.time.LocalDateTime;

public class Parcel {
  private Integer parcelId;
  private double weight;
  private double vol;
  private LocalDateTime sendDate;
  private LocalDateTime receiveDate;
  private Integer fromDep;
  private String descr;
  private Integer payId;
  private Integer sender;
  private String senderName;
  private Integer recipient;
  private String recipeName;
  private Integer route;
  private Integer toDepartment;

  public Parcel() {
    sendDate = LocalDateTime.now();
    receiveDate = sendDate.plusDays(2);
  }

  public LocalDateTime getSendDate() {
    return sendDate;
  }

  public LocalDateTime getReceiveDate() {
    return receiveDate;
  }

  public Integer getRoute() {
    return route;
  }

  public void setRoute(Integer route) {
    this.route = route;
  }

  public Integer getToDepartment() {
    return toDepartment;
  }

  public void setToDepartment(Integer toDepartment) {
    this.toDepartment = toDepartment;
  }

  public Integer getRecipient() {
    return recipient;
  }

  public void setRecipient(Integer recipient) {
    this.recipient = recipient;
  }

  public String getRecipeName() {
    return recipeName;
  }

  public void setRecipeName(String recipeName) {
    this.recipeName = recipeName;
  }

  public String getSenderName() {
    return senderName;
  }

  public void setSenderName(String senderName) {
    this.senderName = senderName;
  }

  public Integer getParcelId() {
    return parcelId;
  }

  public void setParcelId(Integer parcelId) {
    this.parcelId = parcelId;
  }

  public double getWeight() {
    return weight;
  }

  public void setWeight(double weight) {
    this.weight = weight;
  }

  public double getVol() {
    return vol;
  }

  public void setVol(double vol) {
    this.vol = vol;
  }

  public Integer getFromDep() {
    return fromDep;
  }

  public void setFromDep(Integer fromDep) {
    this.fromDep = fromDep;
  }

  public String getDescr() {
    return descr;
  }

  public void setDescr(String descr) {
    this.descr = descr;
  }

  public Integer getPayId() {
    return payId;
  }

  public void setPayId(Integer payId) {
    this.payId = payId;
  }

  public Integer getSender() {
    return sender;
  }

  public void setSender(Integer sender) {
    this.sender = sender;
  }

  public String toString() {
    return String.format(PARCEL_FORMAT,
        getParcelId(), getSenderName(), getRecipeName(),getReceiveDate());
  }
}
