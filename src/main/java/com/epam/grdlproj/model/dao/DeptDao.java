package com.epam.grdlproj.model.dao;

import com.epam.grdlproj.model.Department;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class DeptDao extends DAO<Department, Integer> {
  private static final String SELECT_ALL =
      "SELECT d.id_dep, d.dep_nr, d.city, d.adress, c.city_name FROM department d ";
  private static final String CITY_JOIN = " INNER JOIN city c ON d.city = c.id_city";
  private static final String INSERT =
      "INSERT INTO department (dep_nr, city, adress) VALUES (?, ?, ?)";
  private static final String CHOOSE =
      "SELECT d.id_dep, d.dep_nr, d.city, d.adress, c.city_name FROM department d " +
          "INNER JOIN city c WHERE id_dep = ?";
  private static final String UPDATE =
      "UPDATE department SET dep_nr = ?, city = ?, adress = ? WHERE id_dep = ?";
  private static final String DELETE = "DELETE FROM department WHERE id_dep = ?";

  public List<Department> getAll() throws SQLException {
    Connection connection = makeDbConnection();
    List<Department> departments = new ArrayList<>();
    try (PreparedStatement statement = connection.prepareStatement(SELECT_ALL + CITY_JOIN)){
      try(ResultSet results = statement.executeQuery()) {
        while(results.next()) {
          Department item = new Department();
          item.setDepId(results.getInt(1));
          item.setDepNum(results.getInt(2));
          item.setCity(results.getInt(3));
          item.setAddress(results.getString(4));
          item.setCityName(results.getString(5));
          departments.add(item);
        }
      }
    }
    return departments;
  }

  public boolean update(Department d) throws SQLException{
    Connection connection = makeDbConnection();
    int count;
    try (PreparedStatement statement = connection.prepareStatement(UPDATE)){
      statement.setInt(1, d.getDepNum());
      statement.setInt(2, d.getCity());
      statement.setString(3, d.getAddress());
      statement.setInt(4, d.getDepId());
      count = statement.executeUpdate();
    }
    return count > 0;
  }
  public Optional<Department> getById(Integer id) throws SQLException {
    Connection connection = makeDbConnection();
    Optional<Department> output = Optional.empty();
    try (PreparedStatement statement = connection.prepareStatement(CHOOSE)) {
      statement.setInt(1, id);
      try (ResultSet results = statement.executeQuery()) {
        while (results.next()) {
          Department item = new Department();
          item.setDepId(results.getInt(1));
          item.setDepNum(results.getInt(2));
          item.setCity(results.getInt(3));
          item.setAddress(results.getString(4));
          item.setCityName(results.getString(5));
          output = Optional.of(item);
        }
      }
    }
    return  output;
  }

  public boolean delete(Integer id) throws SQLException{
    Connection connection = makeDbConnection();
    int count;
    try (PreparedStatement statement = connection.prepareStatement(DELETE)){
      statement.setInt(1, id);
      count = statement.executeUpdate();
    }
    return count > 0;
  }

  public boolean create(Department d) throws SQLException {
    Connection connection = makeDbConnection();
    int count;
    try (PreparedStatement statement = connection.prepareStatement(INSERT)){
      statement.setInt(1, d.getDepNum());
      statement.setInt(2, d.getCity());
      statement.setString(3, d.getAddress());
      count = statement.executeUpdate();
    }
    return count > 0;
  }
}
