package com.epam.grdlproj.model.dao;

import com.epam.grdlproj.model.City;
import com.epam.grdlproj.model.Parcel;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ParcelDao /*extends DAO<Parcel, Integer> */{
  /*
  private static final String SELECT_ALL =
      "SELECT c.id_city, c.city_name, c.distr_id, d.distr_name FROM city c ";
  private static final String DISTR_JOIN = " INNER JOIN district d USING (distr_id)";
  //private static final String INSERT = "INSERT INTO city (city_name, distr_id) VALUES (?, ?)";
  //private static final String CHOOSE = "SELECT * FROM city WHERE id_city = ?";
  //private static final String UPDATE = "UPDATE city SET city_name = ?, distr_id = ? WHERE id_city= ?";
  //private static final String DELETE = "DELETE FROM city WHERE id_city = ?";

  public List<Parcel> getAll() throws SQLException {
    Connection connection = makeDbConnection();
    List<Parcel> parcels = new ArrayList<>();
    try (PreparedStatement statement = connection.prepareStatement(SELECT_ALL+DISTR_JOIN)){
      try(ResultSet results = statement.executeQuery()) {
        while(results.next()) {
          Parcel item = new Parcel();
          item.setCityId(results.getInt(1));
          item.setCityName(results.getString(2));
          item.setDistr(results.getInt(3));
          item.setInDistr(results.getString(4));
          parcels.add(item);
        }
      }
    }
    return parcels;
  }

  public boolean update(City city) throws SQLException{
    Connection connection = makeDbConnection();
    int count;
    try (PreparedStatement statement = connection.prepareStatement(UPDATE)){
      statement.setString(1, city.getCityName());
      statement.setInt(2, city.getDistr());
      statement.setInt(3, city.getCityId());
      count = statement.executeUpdate();
    }
    return count > 0;
  }
  public Optional<City> getById(Integer id) throws SQLException {
    Connection connection = makeDbConnection();
    Optional<City> output = Optional.empty();
    try (PreparedStatement statement = connection.prepareStatement(CHOOSE)) {
      statement.setInt(1, id);
      try (ResultSet results = statement.executeQuery()) {
        while (results.next()) {
          City item = new City();
          item.setCityId(results.getInt(1));
          item.setCityName(results.getString(2));
          item.setDistr(results.getInt(3));
          output = Optional.of(item);
        }
      }
    }
    return  output;
  }

  public boolean delete(Integer id) throws SQLException{
    Connection connection = makeDbConnection();
    int count;
    try (PreparedStatement statement = connection.prepareStatement(DELETE)){
      statement.setInt(1, id);
      count = statement.executeUpdate();
    }
    return count > 0;
  }

  public boolean create(City city) throws SQLException {
    Connection connection = makeDbConnection();
    int count;
    try (PreparedStatement statement = connection.prepareStatement(INSERT)){
      statement.setString(1, city.getCityName());
      statement.setInt(2, city.getDistr());
      count = statement.executeUpdate();
    }
    return count > 0;
  }

   */
}
