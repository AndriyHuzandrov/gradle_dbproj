package com.epam.grdlproj.model.dao;

import com.epam.grdlproj.model.Client;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ClientDao extends DAO<Client, Integer> {
  private static final String SELECT_ALL = "SELECT * FROM client";
  private static final String INSERT =
      "INSERT INTO client (cl_name, cl_surname, mobile, passport)" +
          " VALUES (?, ?, ?, ?)";
  private static final String CHOOSE = "SELECT * FROM client WHERE client_id = ?";
  private static final String UPDATE =
      "UPDATE client SET cl_name = ?, cl_surname = ?, mobile = ?, passport = ? WHERE client_id = ?";
  private static final String DELETE = "DELETE FROM client WHERE client_id = ?";
  private static final String CHOOSE_PHONE = "SELECT * FROM client WHERE mobile = ?";

  public List<Client> getAll() throws SQLException {
    Connection connection = makeDbConnection();
    List<Client> districts = new ArrayList<>();
    try (PreparedStatement statement = connection.prepareStatement(SELECT_ALL)){
      try(ResultSet results = statement.executeQuery()) {
        while(results.next()) {
          Client item = new Client();
          item.setClientId(results.getInt(1));
          item.setClName(results.getString(2));
          item.setClSurname(results.getString(3));
          item.setMobile(results.getString(4));
          item.setPassport(results.getString(5));
          districts.add(item);
        }
      }
    }
    return districts;
  }
  public boolean update(Client c) throws SQLException{
    Connection connection = makeDbConnection();
    int count;
    try (PreparedStatement statement = connection.prepareStatement(UPDATE)){
      statement.setString(1, c.getClName());
      statement.setString(2, c.getClSurname());
      statement.setString(3, c.getMobile());
      statement.setString(4, c.getPassport());
      statement.setInt(5, c.getClientId());
      count = statement.executeUpdate();
    }
    return count > 0;
  }
  public Optional<Client> getById(Integer id) throws SQLException {
    Connection connection = makeDbConnection();
    Optional<Client> output = Optional.empty();
    try (PreparedStatement statement = connection.prepareStatement(CHOOSE)) {
      statement.setInt(1, id);
      try (ResultSet results = statement.executeQuery()) {
        while (results.next()) {
          Client item = new Client();
          item.setClientId(results.getInt(1));
          item.setClName(results.getString(2));
          item.setClSurname(results.getString(3));
          item.setMobile(results.getString(4));
          item.setPassport(results.getString(5));
          output = Optional.of(item);
        }
      }
    }
    return  output;
  }

  public boolean delete(Integer id) throws SQLException{
    Connection connection = makeDbConnection();
    int count;
    try (PreparedStatement statement = connection.prepareStatement(DELETE)){
      statement.setInt(1, id);
      count = statement.executeUpdate();
    }
    return count > 0;
  }

  public boolean create(Client c) throws SQLException {
    Connection connection = makeDbConnection();
    int count ;
    try (PreparedStatement statement = connection.prepareStatement(INSERT)){
      statement.setString(1, c.getClName());
      statement.setString(2, c.getClSurname());
      statement.setString(3, c.getMobile());
      statement.setString(4, c.getPassport());
      count = statement.executeUpdate();
    }
    return count > 0;
  }

  public Optional<Client> getById(String phone) throws SQLException {
    Connection connection = makeDbConnection();
    Optional<Client> output = Optional.empty();
    try (PreparedStatement statement = connection.prepareStatement(CHOOSE_PHONE)) {
      statement.setString(1, phone);
      try (ResultSet results = statement.executeQuery()) {
        while (results.next()) {
          Client item = new Client();
          item.setClientId(results.getInt(1));
          item.setClName(results.getString(2));
          item.setClSurname(results.getString(3));
          item.setMobile(results.getString(4));
          item.setPassport(results.getString(5));
          output = Optional.of(item);
        }
      }
    }
    return  output;
  }
}
