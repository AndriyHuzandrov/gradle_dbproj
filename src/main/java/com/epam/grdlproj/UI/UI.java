package com.epam.grdlproj.UI;

import com.epam.grdlproj.menu.ClientLoginMenu;
import com.epam.grdlproj.menu.MainMenu;
import com.epam.grdlproj.menu.MaintainMenu;
import com.epam.grdlproj.menu.Menu;
import com.epam.grdlproj.menu.ParcelProcessMenu;
import com.epam.grdlproj.menu.ParcelRedirectMenu;
import com.epam.grdlproj.menu.TrackParcelMenu;
import com.epam.grdlproj.menu.routeene.ClientLoginTxt;
import com.epam.grdlproj.menu.routeene.MainTxt;
import com.epam.grdlproj.menu.routeene.MaintainTxt;
import com.epam.grdlproj.menu.routeene.ParcelProcessTxt;
import com.epam.grdlproj.menu.routeene.Showable;
import com.epam.grdlproj.menu.routeene.TrackTxt;

public class UI {

  public static Menu newMainMenu() {
    Menu m = new MainMenu();
    m.setTxtMenu(new MainTxt());
    return m;
  }

  public static Menu newParcTrackMenu(String phone) {
    Menu m = new TrackParcelMenu();
    m.setTxtMenu(new TrackTxt());
    m.setClient_id(phone);
    return m;
  }

  public static Menu newClientLoginMenu() {
    Menu m = new ClientLoginMenu();
    m.setTxtMenu(new ClientLoginTxt());
    return m;
  }

  public static Menu newRedirectMenu(Showable txtMenu, String client_owner, String parc_id) {
    Menu m = new ParcelRedirectMenu();
    m.setClient_id(client_owner);
    m.setParcel_id(parc_id);
    m.setTxtMenu(txtMenu);
    return m;
  }

  public static Menu newMaintainMenu() {
    Menu m = new MaintainMenu();
    m.setTxtMenu(new MaintainTxt());
    return m;
  }

  public static Menu newParcelProcesMenu() {
    Menu m = new ParcelProcessMenu();
    m.setTxtMenu(new ParcelProcessTxt());
    return m;
  }
}
