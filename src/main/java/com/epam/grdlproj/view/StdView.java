package com.epam.grdlproj.view;

import static com.epam.grdlproj.txtconst.TxtConsts.*;

public class StdView implements Displayable {

  public void display(String data) {
    appLog.info(data);
  }
}
