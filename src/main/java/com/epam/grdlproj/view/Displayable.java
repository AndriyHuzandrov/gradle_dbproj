package com.epam.grdlproj.view;

public interface Displayable {
  void display(String data);
}
